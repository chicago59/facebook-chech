using System.Collections.Generic;
using Game.Managers;
using UnityEngine;
using UnityEngine.UI;

public class Entry : MonoBehaviour
{
    public Button btnLogin;
    public Button btnLogout;
    public FriendsItemView friendItem;
    public ScrollRect scrollRect;

    private FacebookManager _fbManager;
    private void Awake()
    {
        _fbManager = new FacebookManager();
        _fbManager.FRIENDS_LIST_RECEIVED += OnFriendsListReceived;
    }

    private void OnFriendsListReceived(List<string> obj)
    {
        foreach (var item in obj)
        {
            var view= GameObject.Instantiate(friendItem.gameObject, scrollRect.content, false);
            view.SetActive(true);
            view.GetComponent<FriendsItemView>().Name = item;
        }
    }

    private void OnEnable()
    {
        btnLogin.onClick.AddListener(OnClick);
        btnLogout.onClick.AddListener(OnLogout);
    }

    private void OnLogout()
    {
        _fbManager.InitAndLogout();
    }

    private void OnClick()
    {
        _fbManager.Login();
    }
}
