﻿using Facebook.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using AccessToken = Facebook.Unity.AccessToken;
using Debug = UnityEngine.Debug;

namespace Game.Managers
{
    public enum LoginAppType
    {
        Guest,
        Facebook,
        Google,
    }

    public sealed class FacebookManagerModel
    {
        public string AccessToken;
        public string Id;
    }

    public sealed class FacebookManager
    {
        public struct LoginFacebookUserData
        {
            public string AuthKey;
            public string Name;
        }

        private const string kLocalizationKeyPermissionRequired = "permission_required";
        private const string kLocalizationKeySomethingWentWrong = "something_went_wrong";
        private readonly string[] _accessTokenRequiredPermissions = new[] { "public_profile", "user_friends" };

        public Action<LoginFacebookUserData> LOGEDIN_SUCCESSFULLY;
        public Action LOGEDOUT;
        public Action ERROR_OCCURE;
        public Action LOGIN;
        public Action<List<string>> FRIENDS_LIST_RECEIVED;


        private FacebookManagerModel _model;

        public bool IsLoggedIn
        {
            get
            {
                try
                {
                    return FB.IsInitialized && FB.IsLoggedIn;
                }
                catch (Exception e)
                {
                    Debug.Log($"error: {e}");
                    ERROR_OCCURE?.Invoke();
                }
                return false;
            }
        }

        public FacebookManagerModel Model => _model;


        public void FireLogin()
        {
            LOGIN?.Invoke();
        }

        public void Login()
        {
            try
            {
                if (!FB.IsInitialized)
                {
                    FB.Init(FBInitCallback, OnFBHideCallback);
                }
                else
                {
                    FB.ActivateApp();
                    FBInitCallback();
                }
            }
            catch (Exception e)
            {
                Debug.Log($"error: {e}");
                ERROR_OCCURE?.Invoke();
            }
        }

        private void FBInitCallback()
        {
            try
            {
                if (FB.IsLoggedIn)
                {
                    FetchData();
                }
                else
                {
                    FB.LogInWithReadPermissions(_accessTokenRequiredPermissions, AuthCallback);
                }
            }
            catch (Exception e)
            {
                Debug.Log($"error: {e}");
                ERROR_OCCURE?.Invoke();
            }
        }

        private void OnFBHideCallback(bool arg)
        {
            Debug.Log($"Not all permission granted");
        }

        private void AuthCallback(ILoginResult result)
        {
            if (result.Cancelled)
            {
                Debug.Log($"Not all permission granted");
                return;
            }

            try
            {
                foreach (string perm in _accessTokenRequiredPermissions)
                {
                    if (AccessToken.CurrentAccessToken?.Permissions.Contains(perm) == false)
                    {
                        Debug.Log($"Not all permission granted");
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.Log($"error: {e}");
                ERROR_OCCURE?.Invoke();
            }

            FetchData();
        }

        private void FetchData()
        {
            try
            {
                FB.API("me?fields=picture, friends, first_name, last_name", HttpMethod.GET, FetchDataCallback);
            }
            catch (Exception e)
            {
                Debug.Log($"error: {e}");
                ERROR_OCCURE?.Invoke();
            }
        }

        private void CreateModel()
        {
            try
            {
                _model = new FacebookManagerModel();
                _model.AccessToken = AccessToken.CurrentAccessToken.TokenString;
                Save();
            }
            catch (Exception e)
            {
                Debug.Log($"error: {e}");
                ERROR_OCCURE?.Invoke();
            }
        }

        private void Save()
        {
        }

        private void FetchDataCallback(IGraphResult result)
        {
            try
            {
                Debug.Log($"response: {result.RawResult}");

                //var dictResp = (Dictionary<string, object>)Facebook.MiniJSON.Json.Deserialize(result.RawResult);
                //var friends = (Dictionary<string, object>)dictResp["friends"];
                //var friendsList = (List<object>)friends["data"];
                //List<string> friendsNames = new List<string>();
                //foreach (var item in friendsList)
                //{
                //    string name = (((Dictionary<string, object>) item)["name"]).ToString();
                //    friendsNames.Add(name);
                //}

                //FRIENDS_LIST_RECEIVED?.Invoke(friendsNames);

            }
            catch (Exception e)
            {
                Debug.Log($"error: {e}");
                ERROR_OCCURE?.Invoke();
            }
        }

        private void Logout()
        {
            try
            {
                FB.LogOut();
                LOGEDOUT?.Invoke();
            }
            catch (Exception e)
            {
                Debug.Log($"error: {e}");
                ERROR_OCCURE?.Invoke();
            }
        }

        public void InitAndLogout()
        {
            try
            {
                if (!FB.IsInitialized)
                {
                    FB.Init(FBInitCallbackLogout, FBInitFailedLogoutCallback);
                }
                else
                {
                    FB.ActivateApp();
                    Logout();
                }
            }
            catch (Exception e)
            {
                Debug.Log($"error: {e}");
                ERROR_OCCURE?.Invoke();
            }
        }

        private void FBInitFailedLogoutCallback(bool arg)
        {
            ERROR_OCCURE?.Invoke();
        }

        private void FBInitCallbackLogout()
        {
            Logout();
        }
    }
}