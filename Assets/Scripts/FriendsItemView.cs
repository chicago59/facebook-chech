﻿using TMPro;
using UnityEngine;

namespace Game.Managers
{
    public class FriendsItemView : MonoBehaviour
    {
        public TextMeshProUGUI _name;

        public string Name
        {
            get => _name.text;
            set => _name.text = value;
        }
    }
}